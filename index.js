import {createApi} from 'unsplash-js';
import express from 'express'
import fetch from 'node-fetch'
import cors from 'cors'

globalThis.fetch = fetch

let app = express();

let unsplash = createApi({
    accessKey: "mev_QkRy5yOtYQyfqeiqkUrqOtSqB2ucQWFqht_Nf3Q"
});

app.use(express.json());
app.use(cors());

app.post('/search', (res, rep) => {
    //DOWNLOAD FILE TO LOCAL
    const token = res.body.basic_token
    if(token) {
        let limit = res.body.limit
        let page = res.body.page
        let query = res.body.query
        unsplash.search.getPhotos({
            query: query ,
            page: page,
            perPage: limit
        }).then(splashResponse => {
            rep.json({
                response: splashResponse.response
            });
        })
    } else {
        rep.json({
            content: 'NOT ALLOWED'
        });
    }
})

app.get('/', (res, rep) => {
    rep.json({
        content: 'This is a test'
    });
})

app.listen(80);
console.log("Server has started.");