FROM node:14

WORKDIR /usr/src/app

COPY --chown=www-data:www-data . .

RUN chmod -R 775 /usr/src/app

EXPOSE 80
EXPOSE 443
EXPOSE 2222

CMD ["node", "index.js"]

